
from appBikes.models.products import Products
from rest_framework import serializers

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['idproduct', 'codeproduct', 'nombre','description']