from rest_framework import serializers
from appBikes.models.user import User 
#from appBikes.models.products import Products
#from appBikes.models.order import Order
#from appBikes.serializers.productsSerializer import ProductsSerializer
#from appBikes.serializers.orderSerializer import OrderSerializer

class UserSerializer(serializers.ModelSerializer):
    #Order = OrderSerializer()
    class Meta:
        model = User
        fields = ['iduser', 'email', 'password', 'name', 'phone', 'address']

    #def create(self, validated_data):
    #    accountData = validated_data.pop('order')
    #    userInstance = User.objects.create(**validated_data)
    #    Account.objects.create(user=userInstance, **accountData)
    #    return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        #order = Order.objects.get(user=obj.id)  
        #account = Account.objects.get(user=obj.id)       
        return {
                    'iduser': user.iduser, 
                    'email': user.email,
                    'password':user.password,
                    'name': user.name,
                    'phone':user.phone,
                    'address':user.address,
                    }
                
