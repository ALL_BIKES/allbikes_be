from django.db import models
from django.db.models.fields import DateTimeField
from .user import User
from .products import Products

class Order(models.Model):
    idorder = models.AutoField(primary_key=True)
    iduser = models.ForeignKey(User, on_delete=models.CASCADE)
    idproduct = models.ForeignKey(Products, on_delete=models.CASCADE)
    date = models.DateTimeField()