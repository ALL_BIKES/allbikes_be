from django.db import models


class Products(models.Model):
    idproduct = models.AutoField(primary_key=True)
    codeproduct=models.CharField('Code', max_length = 15)
    nombre=models.CharField('Name',max_length=15)
    description=models.CharField('Description',max_length=30)
    