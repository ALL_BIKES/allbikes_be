from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
            def create_user(self, email, password=None):
                if not email:
                    raise ValueError('Users must have an username')
                user = self.model(email=email)
                user.set_password(password)
                user.save(using=self._db)
                return user

            def create_superuser(self, email, password):
                user = self.create_user(
                    email=email,
                    password=password,
                )
                user.is_admin = True
                user.save(using=self._db)
                return user

class User(AbstractBaseUser, PermissionsMixin):
    iduser = models.BigAutoField(primary_key=True)
    email = models.EmailField('Email', unique=True, max_length = 100)
    password = models.CharField('Password', max_length = 256)
    name = models.CharField('Name', max_length = 30)
    phone = models.CharField('Phone', max_length = 15)
    address = models.CharField('Address', max_length = 30)
    

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'  
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'email'
