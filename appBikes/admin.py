from django.contrib import admin

# Register your models here.

from .models.user import User
from .models.order import Order
from .models.products import Products

admin.site.register(User)
admin.site.register(Order)
admin.site.register(Products)